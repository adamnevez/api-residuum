import { Router, Request, Response } from "express"
import { getUsers, saveUser, getUser, updateUser, superUser, removeUser } from "./controller/UserController"

const routes = Router()

routes.get('/', (request: Request, response: Response) => {
    return response.json({ message: 'hello world' })
})

// routes users 
routes.get('/user', getUsers)
routes.get('/user/:id', getUser)
routes.post('/user', saveUser)
routes.put('/user/:id', updateUser)
routes.delete('/user/:id', removeUser)
routes.patch('/user/:id', superUser)

export default routes;