import { getRepository } from "typeorm"
import { User } from '../entity/User'
import { Request, Response } from "express"

export const getUsers = async (request: Request, response: Response) => {

    const user = await getRepository(User).find()
    return response.json(user)
};

export const getUser = async (request: Request, response: Response) => {

    const { id } = request.params
    const user = await getRepository(User).findOne(id)
    return response.json(user)
};

export const saveUser = async (request: Request, response: Response) => {
    const user = await getRepository(User).save(request.body)
    return response.json(user)
};

export const updateUser = async (request: Request, response: Response) => {

    const { id } = request.params
    const user = await getRepository(User).update(id,request.body)

    if(user.affected === 1) {
        const userUpdated = await getRepository(User).findOne(id)
        return response.json(userUpdated)
    }

    return response.status(404).json({ message: 'not found'})
};

export const superUser = async (request: Request, response: Response) => {

    const { id } = request.params
    const user = await getRepository(User).update(id,{
        super_user: true
    })

    if(user.affected === 1) {
        const userUpdated = await getRepository(User).findOne(id)
        return response.json({ message: 'its super user' })
    }

    return response.status(404).json({ message: 'not found'})
};

export const removeUser = async (request: Request, response: Response) => {

    const { id } = request.params
    const user = await getRepository(User).delete(id)

    if(user.affected === 1) {
        const userUpdated = await getRepository(User).findOne(id)
        return response.json({ message: 'user removed' })
    }

    return response.status(404).json({ message: 'not found'})
};