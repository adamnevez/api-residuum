import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn  } from "typeorm";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name_user: string;

    @Column()
    email: string;

    @Column({
        default:false
    })
    super_user: boolean;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;
}
